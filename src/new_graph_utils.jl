function new_graph(config_query::Socket)
    gc(x) = get_config_x(config_query,x)

    pairs_dict_raw = gc("pairs")
    blacklist = gc("blacklist")
    fees = Dict(Symbol(k)=>v for (k,v) in gc("fees"))
    amounts = Dict(Symbol(k)=>v for (k,v) in gc("amounts"))

    pairs_dict = Dict{Symbol, Array{Tuple{Symbol, Symbol},1}}()
    exchanges = Array{Symbol,1}()
    exch_cur = Dict{Symbol, Array{Symbol,1}}()
    cur_exch = Dict{Symbol, Array{Symbol,1}}()
    graph_index = Dict{Tuple{Symbol,Symbol}, Int64}()
    rev_index = Dict{Int64, Tuple{Symbol,Symbol}}()
    for (k,v) in pairs_dict_raw
        bl = get(blacklist, k , [])
        str_list = filter(x-> x ∉ bl, v)
        plist = [(x[1],x[2]) for x in map(x->map(Symbol, split(x,"-")), str_list)]
        cur_list = vcat([x[1] for x in plist], [x[2] for x in plist]) |> union
        exchange = k |> Symbol
        pairs_dict[exchange] = plist
        exch_cur[exchange] = cur_list
        push!(exchanges, exchange)

        for c in cur_list
            i = length(graph_index)
            graph_index[(exchange, c)] = i+1
            rev_index[i+1] = (exchange, c)
            arr = get(cur_exch, c, [])
            push!(arr, exchange)
            cur_exch[c] = arr
        end
    end
    all_pairs = pairs_dict |> values |> Iterators.flatten |> union
    currencies = cur_exch |> keys |> collect

    g = SimpleDiGraph()
    g_n = length(graph_index)
    add_vertices!(g, g_n)
    path_ind = Dict()

    for (exchange, pairs_list) in pairs_dict
        for pair in pairs_list
            v1 = graph_index[(exchange, pair[1])]
            v2 = graph_index[(exchange, pair[2])]
            path_ind[v1=>v2]=(:trade, pair, exchange, :sell, :bid, :curr => :quote)
            add_edge!(g, v1, v2)
            path_ind[v2=>v1] =(:trade, pair, exchange, :buy, :ask, :curr => :base)
            add_edge!(g, v2, v1)
        end
    end

    for (cur, exch_list) in cur_exch
        combs = collect(combinations(exch_list,2))
        for comb in combs
            v1 = graph_index[(comb[1], cur)]
            v2 = graph_index[(comb[2], cur)]
            path_ind[v1=>v2] = (:transfer, cur, comb[1] => comb[2])
            add_edge!(g, v1, v2)
            #weight_matrix[v1,v2] = 1.0
            path_ind[v2=>v1] = (:transfer, cur, comb[2] => comb[1])
            add_edge!(g, v2, v1)
            #weight_matrix[v2,v1] = 1.0
        end
    end

    cycle_list = simplecycles_limited_length(g, 8)
    filter!(x->length(x)>2,cycle_list)

    function test_false_steps(cycle::Array{Int64,1}, path_index::Dict)
        z = zip(cycle, circshift(cycle, -1))

        step_types = Vector{Symbol}()

        for (i,j) in z
            push!(step_types, path_index[i=>j][1])
        end

        res = true
        for (i,j) in zip(step_types, circshift(step_types, -1))
            if i == :transfer && i == j
                res = res & false
            end
        end
        res
    end

    filter!(x->test_false_steps(x,path_ind), cycle_list)

    function  verbose_cycle(cycle::Array{Int64,1}, rev_index::Dict{Int64,Tuple{Symbol,Symbol}}, pd::Dict{Symbol,Array{Tuple{Symbol,Symbol},1}})
        verbs = map(cycle) do i
            rev_index[i]
        end

        ff = filter(collect(zip(verbs, circshift(verbs, -1)))) do x
            x[1][1] == x[2][1]
        end

        pairs = map(ff) do x
            (x[1][1],(x[1][2], x[2][2]))
        end

        arr = Vector{Tuple{Symbol, Symbol, Tuple{Symbol, Symbol}}}()
        for pa in pairs
            e, sym = pa
            out = sym in pd[e] ? (:buy, e, sym) : (:sell, e, reverse(sym))
            push!(arr, out)
        end
        arr
    end

    arb_orders = map(cycle_list) do cycle
        verbose_cycle(cycle, rev_index, pairs_dict)
    end

    function reverse_order(row, graph_index)
        out = Array{Tuple{Int64,Int64},1}()
        for x in row
            p1 = (x[2],x[3][2])
            p2 = (x[2],x[3][1])
            points = x[1] == :buy ? (p1, p2) : (p2, p1)
            pp = map(points) do x
                graph_index[x]
            end
            push!(out, pp)
        end
        out
    end

    cycle_dict_all = Dict{Int64,Array{Tuple{Int64,Int64},1}}()
    arbs_all = Dict{Int64, Array{Tuple{Symbol,Symbol,Tuple{Symbol,Symbol}},1}}()
    for (i,x) in enumerate(arb_orders)
        cycle_dict_all[i] = reverse_order(x, graph_index)
        arbs_all[i] = x
    end

    rev_path_index = Dict{Tuple{Symbol, Tuple{Symbol,Symbol}}, Pair{Int64,Int64}}()
    li = Array{Tuple{Symbol,Symbol,Tuple{Symbol,Symbol}},1}()
    pa = Dict{Pair{Int64, Int64},Tuple{Symbol, Symbol, Tuple{Symbol, Symbol}}}()
    for (k,v) in path_ind
        if v[1] == :trade
            rev_path_index[(v[3],v[2])] = k
            row = (v[4], v[3], v[2])
            push!(li, row)
            pa[k] = row
        end
    end

    ddd = Dict{Tuple{Symbol, Symbol, Tuple{Symbol, Symbol}}, Vector{Int64}}()
    arb_index = Dict{Int64,Array{Tuple{Symbol,Symbol, Tuple{Symbol,Symbol}}}}()
    for (k_arb, v_array) in arbs_all
        for k in li
            if k in v_array
                d_i = get(ddd, k, Vector{Int64}())
                push!(d_i, k_arb)
                ddd[k] = d_i
                arb_index[k_arb] = v_array
            end
        end
    end

    ConstGraph(
        pa,
        rev_path_index,
        li,
        ddd,
        cycle_dict_all,
        graph_index,
        arb_index,
        fees,
        amounts,
        pairs_dict,
        cur_exch
    )
end

function feather(cg::ConstGraph)
    matrix_labels = [:weight_matrix, :bid_ask]
    g_n = length(cg.graph_index)
    for m in matrix_labels
        exp = :($m = (zeros(Float64, $g_n, $g_n)))
        eval(exp)
    end

    for (cur, exch_list) in cg.cur_exch
        combs = collect(combinations(exch_list,2))
        for comb in combs
            v1 = cg.graph_index[(comb[1], cur)]
            v2 = cg.graph_index[(comb[2], cur)]
            weight_matrix[v1,v2] = 1.0
            weight_matrix[v2,v1] = 1.0
        end
    end

    time_log = zeros(Float64, size(weight_matrix))
    Feather(
        weight_matrix,
        bid_ask,
        time_log
    )
end

function get_cycle_returns(cyc::Array{Tuple{Int64,Int64},1}, wm::SparseMatrixCSC{Float64,Int64})
    foldl(*, map(x->wm[x],cyc))
end

function lookup_constant(ag::ExpressiveGraph)
    rev_path_index = Dict{Tuple{Symbol, Tuple{Symbol,Symbol}}, Pair{Int64,Int64}}()
    li = Array{Tuple{Symbol,Symbol,Tuple{Symbol,Symbol}},1}()
    pa = Dict{Pair{Int64, Int64},Tuple{Symbol, Symbol, Tuple{Symbol, Symbol}}}()
    for (k,v) in ag.path_index
        if v[1] == :trade
            rev_path_index[(v[3],v[2])] = k
            row = (v[4], v[3], v[2])
            push!(li, row)
            pa[k] = row
        end
    end

    ddd = Dict{Tuple{Symbol, Symbol, Tuple{Symbol, Symbol}}, Vector{Int64}}()
    arb_index = Dict{Int64,Array{Tuple{Symbol,Symbol, Tuple{Symbol,Symbol}}}}()
    for (k_arb, v_array) in ag.arbs_all
        for k in li
            if k in v_array
                d_i = get(ddd, k, Vector{Int64}())
                push!(d_i, k_arb)
                ddd[k] = d_i
                arb_index[k_arb] = v_array
            end
        end
    end
    cda = copy(ag.cycle_dict_all)
    agi = copy(ag.graph_index)
    fees = copy(ag.fees)
    amounts = copy(ag.amounts)

    ConstGraph(
        pa,
        rev_path_index,
        li,
        ddd,
        cda,
        agi,
        arb_index,
        fees,
        amounts
    )
end

