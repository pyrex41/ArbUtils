module ArbUtils

using LightGraphs, Combinatorics, SparseArrays, ZMQ, JSON, Dates, HTTP,
     ThreadsX, OrderUtils, Base.Threads, SocketUtils, HelperUtils, #ExchangeClients

import Base.copy

export ArbGraph, ExpressiveGraph, LightGraph, DynamicGraph, SnapshotGraph, Feather, ConstGraph,
new_graph, copy, feather, lookup_constant, get_cycle_returns,
fast_update!, fast_list!, calc!, calc_n, calc_n!, fast_process!, fast_calc!, order_info

# ^ not all functions are exported here; adjust as needed

# abstract graph types allow extensions for specific applications that should can
# be compatible with appropriate functions
abstract type ExpressiveGraph end
abstract type LightGraph <: ExpressiveGraph end
abstract type FixedGraph end

include("structures.jl")
include("graph_structs.jl")
include("fast_socket.jl")
include("new_graph_utils.jl")

#include("helper_utils.jl")
#include("test_order_utils.jl")
#include("graph_utils.jl")
#include("SFOXutils.jl")
#include("socket_funcs.jl")

end # module
