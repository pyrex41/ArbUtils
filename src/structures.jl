struct SnapshotGraph <: LightGraph
    weight_matrix::SparseMatrixCSC{Float64,Int64}
    update_matrix::SparseMatrixCSC{Float64,Int64}
    bids::SparseMatrixCSC{Float64,Int64}
    asks::SparseMatrixCSC{Float64,Int64}
    spreads::Dict{Int64,Float64}
    timestamp::Float64
end

struct ArbGraph <: ExpressiveGraph
    graph::SimpleDiGraph
    graph_index::Dict{Tuple{Symbol,Symbol},Int64}
    rev_index::Dict{Int64, Tuple{Symbol,Symbol}}
    pairs_dict::Dict{Symbol, Array{Tuple{Symbol,Symbol},1}}
    weight_matrix::SparseMatrixCSC{Float64,Int64}
    update_matrix::SparseMatrixCSC{Float64,Int64}
    bids::SparseMatrixCSC{Float64,Int64}
    asks::SparseMatrixCSC{Float64,Int64}
    path_index::Dict
    rev_pair_index::Dict
    cycle_list::Array
    arbs_all::Dict
    cycle_dict_all::Dict{Int64,Array{Tuple{Int64,Int64},1}}
    rev_all::Dict{Tuple{Symbol,Tuple{Symbol,Symbol}},Dict}
    spreads::Dict{Int64,Float64}
    fees::Dict{Symbol, Float64}
    amounts::Dict{Symbol, Float64}
end

struct DynamicGraph <: LightGraph
    weight_matrix::SparseMatrixCSC{Float64,Int64}
    update_matrix::SparseMatrixCSC{Float64,Int64}
    bids::SparseMatrixCSC{Float64,Int64}
    asks::SparseMatrixCSC{Float64,Int64}
    spreads::Dict{Int64,Float64}
end

struct Feather <: LightGraph
    weight_matrix::SparseMatrixCSC{Float64,Int64}
    bid_ask::SparseMatrixCSC{Float64,Int64}
    time_log::SparseMatrixCSC{Float64,Int64}
    #spread_list::Array{Int8,1} # don't seem
end

struct ConstGraph <: FixedGraph
    path_index::Dict{Pair{Int64, Int64}, Tuple{Symbol, Symbol, Tuple{Symbol,Symbol}}}
    rev_path_index::Dict{Tuple{Symbol, Tuple{Symbol,Symbol}}, Pair{Int64,Int64}}
    trade_actions::Array{Tuple{Symbol,Symbol,Tuple{Symbol,Symbol}},1}
    spread_lookup::Dict{Tuple{Symbol, Symbol, Tuple{Symbol, Symbol}}, Vector{Int64}}
    cycle_dict_all::Dict{Int64,Array{Tuple{Int64,Int64},1}}
    graph_index::Dict{Tuple{Symbol,Symbol},Int64}
    arb_index::Dict{Int64, Array{Tuple{Symbol,Symbol, Tuple{Symbol,Symbol}},1}}
    fees::Dict{Symbol, Float64}
    amounts::Dict{Symbol, Float64}
    pairs_dict::Dict{Symbol,Array{Tuple{Symbol,Symbol},1}}
    cur_exch::Dict{Symbol, Array{Symbol,1}}
end
